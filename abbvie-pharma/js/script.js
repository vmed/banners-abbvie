"use strict";

window.addEventListener('load', function () {
    document.querySelector('#frame-white').style.opacity = "0";
});
var mainDuration = 800;
var tl = anime.timeline({
    easing: 'easeInOutCubic'
});
tl.add({
    targets: '.main__question',
    top: 130,
    opacity: 1,
    delay: 3000,
    duration: mainDuration,
    begin: function begin() {
        document.querySelector('.references__popup--close').onclick = tl.play;
    }
}).add({
    targets: '.main__question',
    top: 10,
    delay: 3000,
    duration: mainDuration
}).add({
    targets: '.main__answer--one',
    top: 140,
    opacity: 1,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.white-background',
    opacity: 1,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.main__answer--two',
    top: 210,
    opacity: 1,
    duration: mainDuration,
    delay: 1500
}).add({
    targets: '.main__answer--three',
    top: 280,
    opacity: 1,
    duration: mainDuration,
    delay: 2000
}).add({
    targets: '.main__answer--four',
    top: 350,
    opacity: 1,
    duration: mainDuration,
    delay: 2000
}).add({
    targets: '.main__answer--five',
    top: 420,
    opacity: 1,
    duration: mainDuration,
    delay: 2000
}).add({
    targets: '.white-background',
    opacity: 0,
    duration: mainDuration,
    delay: 3000
}).add({
    targets: '.img-background__small-triangle',
    top: 289,
    right: 30,
    duration: mainDuration
}, '-=700').add({
    targets: '.img-background__large-triangle',
    top: 255,
    right: 88,
    duration: mainDuration
}, '-=700').add({
    targets: 'main *',
    top: 0,
    opacity: 0,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.last-screen__more',
    opacity: 1,
    top: 76,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.logo-abbviepro',
    translateY: -340,
    width: 229,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.footer__logo-abbviepro',
    translateY: -340,
    width: 229,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.footer__pharma',
    translateY: -335,
    scale: 1.8,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.last-screen__button',
    top: 255,
    opacity: 1,
    duration: mainDuration,
    complete: function complete() {
        document.querySelector('.references__popup--close').onclick = tl.pause;
    }
}, "-=" + mainDuration); //references
//animation stop when references are clicked

document.querySelector('.footer__references').onclick = tl.pause;
document.querySelector('.footer__references').onclick = function () {
    document.querySelector('.references__bcg').style.opacity = "1";
    document.querySelector('.references__popup').style.opacity = "1";
    document.querySelector('footer').style.zIndex = "-1";
    document.querySelector('.references').style.zIndex = "10";
    document.querySelector('.last-screen__button').style.zIndex = "-1";
};
document.querySelector('.references__popup--close').addEventListener("click", function () {
    document.querySelector('.references__bcg').style.opacity = "-2";
    document.querySelector('.references__popup').style.opacity = "-2";
    document.querySelector('footer').style.zIndex = "inherit";
    document.querySelector('.references').style.zIndex = "-3";
    document.querySelector('.last-screen__button').style.zIndex = "1";
});