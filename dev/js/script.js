"use strict";

window.addEventListener('load', function () {
    document.querySelector('#frame-white').style.opacity = "0";
}); //duration

var mainDuration = 800;
var personDuration = 600;
var mainDelay = 3000;
var tl = anime.timeline({
    easing: 'easeInOutCubic'
});
tl.add({
    targets: '.main__question',
    top: 191,
    opacity: 1,
    delay: 2000,
    duration: mainDuration
}).add({
    targets: '.main__question',
    top: 10,
    delay: mainDelay,
    duration: mainDuration
}).add({
    targets: '.img-background__small-triangle',
    left: 234,
    top: 166,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.img-background__large-triangle',
    left: -21,
    top: 212,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.main__presentation',
    opacity: 1,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.person-one',
    translateX: -250,
    opacity: 0,
    delay: mainDelay,
    duration: mainDuration
}).add({
    targets: '.img-background__small-triangle',
    left: 245,
    top: 280,
    rotate: -60,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.img-background__large-triangle',
    left: -10,
    top: 165,
    rotate: -50,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.person-two',
    keyframes: [{
        left: 54,
        opacity: 1,
        duration: mainDuration
    }, {
        left: -300,
        opacity: 0,
        duration: mainDuration,
        delay: mainDelay
    }]
}, "-=" + mainDuration).add({
    targets: '.img-background__small-triangle',
    left: 236,
    top: 135,
    rotate: 86,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.img-background__large-triangle',
    left: -25,
    top: 220,
    rotate: -36,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.person-three',
    keyframes: [{
        left: 54,
        opacity: 1,
        duration: mainDuration
    }, {
        left: -300,
        opacity: 0,
        duration: mainDuration,
        delay: mainDelay
    }]
}, "-=" + mainDuration).add({
    targets: '.img-background__small-triangle',
    rotate: 0,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.img-background__large-triangle',
    rotate: 0,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.person-four',
    keyframes: [{
        left: 54,
        opacity: 1,
        duration: mainDuration
    }, {
        left: -300,
        opacity: 0,
        duration: mainDuration,
        delay: mainDelay
    }]
}, "-=" + mainDuration).add({
    targets: '.img-background__small-triangle',
    left: 236,
    top: 85,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.img-background__large-triangle',
    left: -21,
    top: 376,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.main__question',
    top: -130,
    opacity: 0,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.main__presentation--discover',
    opacity: 0,
    top: 300,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.last-screen__more',
    top: 139,
    opacity: 1,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.last-screen__button',
    top: 321,
    opacity: 1,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.footer__logo-abbviepro',
    translateY: -280,
    width: 230,
    duration: mainDuration
}, "-=" + mainDuration).add({
    targets: '.footer__pharma',
    translateY: -276,
    scale: 1.7,
    duration: mainDuration
}, "-=" + mainDuration);